/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// Les imports importants
import React, { useState } from "react";
import ReactDOM from "react-dom";
import {
    HashRouter, Route, Switch, withRouter
} from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';

// any CSS you import will output into a single css file (app.css in this case)
import "./styles/app.css";

// start the Stimulus application
import "./bootstrap";
import Navbar from "./js/components/navbar";
import PrivateRoute from "./js/components/PrivateRoute";
import AuthContext from "./js/context/AuthContext";
import CustomersPage from "./js/pages/CustomersPage";
import HomePage from "./js/pages/HomePage";
import InvoicesPage from "./js/pages/InvoicesPage";
import LoginPage from "./js/pages/LoginPage";
import AuthApi from "./js/services/AuthApi";
import CustomerPage from "./js/pages/CustomerPage";
import InvoicePage from "./js/pages/InvoicePage";
import RegisterPage from "./js/pages/RegisterPage";
import { ToastContainer } from "react-toastify";
import { toast } from "react-toastify";


AuthApi.SetUp();


const App = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(
    AuthApi.isAuthenticated()
  );

  const NavbarWithRouter = withRouter(Navbar);
  return (
    <>
      <AuthContext.Provider value={{
    isAuthenticated,
    setIsAuthenticated
    }}>
        <HashRouter>
          <NavbarWithRouter />
          <main>
            <Switch>
              <Route
                path="/login"
                component={LoginPage}
              ></Route>
              <Route path="/register" component={RegisterPage}></Route>
               <PrivateRoute
                path="/invoices/:id"
                component={InvoicePage}
              />
              <PrivateRoute
                path="/invoices"
                component={InvoicesPage}
              />
              <PrivateRoute
                path="/customers/:id"
                component={CustomerPage}
              />
              <PrivateRoute
                path="/customers"
                component={CustomersPage}
              />
              <Route path="/" component={HomePage}></Route>
            </Switch>
          </main>
        </HashRouter>
        <ToastContainer position={toast.POSITION.BOTTOM_LEFT}/>
      </AuthContext.Provider>
    </>
  );
};

const rootElement = document.querySelector("#app");
ReactDOM.render(<App />, rootElement);
