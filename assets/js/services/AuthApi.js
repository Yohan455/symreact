import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { LOGIN_API } from '../config';

/**
 * Positionne le token JWT sur Axios
 * @param {string} token 
 */
function setAxiosToken(token){
    axios.defaults.headers["Authorization"] = "Bearer " + token;
}

/**
 * Requete HTTP d'authentification et stckage du token dans le storage
 * @param {object} credentials 
 * @returns 
 */
function Authenticate (credentials) {
    return axios 
    .post(LOGIN_API, credentials)
    .then(response => response.data.token)
    .then(token => {
        window.localStorage.setItem("authToken", token);
        setAxiosToken(token);
    });

   
}

/**
 * Agit lors de la déconnexion
 */
function Logout() {
    window.localStorage.removeItem('authToken');
    delete axios.defaults.headers['Authorization'];
}

/**
 * Mise en place au chargement de l'application
 * 
 */
function SetUp() {
    const token = window.localStorage.getItem('authToken');

    if(token) {
        const {exp:expiration} = jwtDecode(token);
        if(expiration * 1000 > new Date().getTime()) {
            setAxiosToken(token);
        }
    } 
    
}

/**
 * permet de savoir si on est authentifié
 * @returns 
 */
function isAuthenticated() {
    const token = window.localStorage.getItem('authToken');

    if(token) {
        const {exp:expiration} = jwtDecode(token);
        if(expiration * 1000 > new Date().getTime()) {
            return true;
        }
        return false;
    } else {
        return false;
    }
}

export default {
    Authenticate,
    Logout,
    SetUp,
    isAuthenticated
}