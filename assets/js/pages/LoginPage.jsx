import React, { useContext, useState } from "react";
import { toast } from "react-toastify";
import Field from "../components/forms/Field";
import AuthContext from "../context/AuthContext";
import AuthApi from "../services/AuthApi";

const LoginPage = ({ history }) => {
  const [credentials, setCredentials] = useState({
    username: "",
    password: "",
  });
  const [error, setError] = useState("");
  const { setIsAuthenticated } = useContext(AuthContext);

  //Gestion des champs
  const handleChange = ({ currentTarget }) => {
    const { value, name } = currentTarget;
    setCredentials({ ...credentials, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      await AuthApi.Authenticate(credentials);
      setError("");
      setIsAuthenticated(true);
      toast.success("Vous êtes désormais connecté 🤙 !");
      history.replace("/customers");
    } catch (error) {
      console.log(error);
      setError(
        "Aucun compte ne possède cette adresse email ou alors les informations ne correpondent pas "
      );
      toast.error('Il semble y avoir quelque chose qui cloche 🤨')
    }
  };

  return (
    <>
      <div className="container my-5">
        <h1 className="text-center">Connexion</h1>
        <div className="d-flex justify-content-center">
          <form onSubmit={handleSubmit} className="w-50">
            <Field
              name="username"
              label="Adresse Email"
              type="email"
              onChange={handleChange}
              value={credentials.username}
              error={error}
              placeholder="user@gmail.com"
            />
            <Field
              name="password"
              label="Mot de passe"
              type="password"
              onChange={handleChange}
              value={credentials.password}
              error=""
              placeholder="********"
            />
            <div className="form-group mb-3">
              <div className="d-grid gap-2">
                <button className="btn btn-primary" type="submit">
                  Connexion
                </button>
              </div>
              <p className="text-center mt-3">
                Pas encore de compte ?{" "}
                <button type="button" class="btn btn-link">
                  S'enregistrer
                </button>
              </p>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default LoginPage;
