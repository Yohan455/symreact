import React, {useState,useEffect} from 'react';
import Pagination from '../components/Pagination';
import moment from 'moment/moment';
import InvoicesApi from '../services/InvoicesApi';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import TableLoader from '../loaders/Tableloader';



const InvoicesPage = () => {
    const [loading, setLoading] = useState(true);
    const [invoices, setInvoices] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const itemsPerPage = 10;

    const STATUS_CLASSES = {
        PAID : "success",
        CANCELLED : "danger",
        SENT : "dark"
    }

    const STATUS_LABELS = {
        PAID: "PAYÉE",
        CANCELLED : "ANNULÉE",
        SENT : "ENVOYÉE"
    }
    

    
    const fetchInvoices = async () => {
        try {
            const data = await InvoicesApi.findAll();
            setInvoices(data);
        } catch (error) {
            console.log(error.response.data.code);
            if (error.response.data.code) {
                toast.error('Erreur lors du chargement des factures ')
                history.pushState('login');
            }

        }
    }

    const handleSearch = ({currentTarget}) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
   }

   const filteredInvoices = invoices.filter(
    i =>
        i.customer.firstName.toLowerCase().includes(search.toLowerCase()) ||
        i.customer.lastName.toLowerCase().includes(search.toLowerCase())   ||
        i.amount.toString().includes(search.toLowerCase()) ||
        STATUS_LABELS[i.status].toLowerCase().includes(search.toLowerCase()));


   const paginatedInvoices = Pagination.getData(filteredInvoices, currentPage, itemsPerPage);


   // Gestion du changement de page 
   const handlePageChange = page => setCurrentPage(page);

    useEffect(() => {
            fetchInvoices();
            setLoading(false);
    }, [])

    const formatDate = (str) => moment(str).format('DD/MM/YYYY');

     // Gestion de la suppression d'un client 
   const handleDelete = async id => {
    const originalsInvoices = [...invoices];

    setInvoices(invoices.filter(invoice => invoice.id !== id));

    try {
        await InvoicesApi.delete(id);
        toast.success('La facture a bien été supprimée !')
    } catch (error) {
        toast.error('Il semble y avoir quelque chose qui cloche 🤨')
        setInvoices(originalsInvoices);
    }
   }

    return ( 
        <>
         <div className="container py-5">
         <div className="d-flex justify-content-between align-items-center mb-3">
            <h1>Factures</h1>
            <Link className='btn btn-primary' to={'/invoices/new'}>Ajouter une Facture</Link>
        </div>
            <div className="form-group">
                <input type="text" onChange={handleSearch} value={search} className="form-control search" placeholder="🔍 Rechercher..." />
            </div>
            {loading && <TableLoader />}
            {!loading &&
            <table className='table table-hover my-5'>
            <thead className='bg-dark text-light'>
                <tr>
                    <th scope='col'>Numéro</th>
                    <th scope='col'>Client</th>
                    <th scope='col'>Date d'envoi</th>
                    <th scope='col'>Statut</th>
                    <th scope='col' className='text-center'>Montant</th>
                    <th scope='col' className='text-center'>Actions</th>
                </tr>
            </thead>
            <tbody>
                {!loading && paginatedInvoices.map(invoice =>  
                <tr key={invoice.id}>
                        <th scope='row'>{invoice.chrono}</th>
                         <td>
                             <Link to={"/customers/" + invoice.customer.id}>{invoice.customer.firstName} {invoice.customer.lastName}</Link>
                         </td>
                         <td>{formatDate(invoice.sentAt)}</td>
                        
                         <td>
                             <span className={"badge bg-" + STATUS_CLASSES[invoice.status]}>{STATUS_LABELS[invoice.status]}</span>
                         </td>
                         <td className='text-center'>{invoice.amount.toLocaleString()}€</td>
                         <td>
                             <Link to={'/invoices/' + invoice.id} className= "btn btn-light"><i className="bi bi-pencil-square"></i>&nbsp;Editer</Link>
                             <a onClick={() => handleDelete(invoice.id)} className= "btn btn-danger"><i className="bi bi-trash-fill"></i>&nbsp;Supprimer</a>
                         </td>
                </tr>
                )}
            </tbody>
            </table>
            }
            {itemsPerPage < filteredInvoices.length && 
            <Pagination currentPage={currentPage} 
            itemsPerPage={itemsPerPage} 
            length={filteredInvoices.length} 
            onPageChanged={handlePageChange}/>}
          </div>
        </>
     );
}
 
export default InvoicesPage;