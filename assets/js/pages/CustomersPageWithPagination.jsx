import React, {useState,useEffect} from 'react';
import axios from 'axios';
import Pagination from '../components/Pagination';


const CustomersPageWithPagination = () => {

   const [customers, setCustomers] = useState([]);
   const [totalItems, setTotalItems] = useState(0);
   const [loading, setLoading] = useState(true);
   const [currentPage, setCurrentPage] = useState(1);

   const itemsPerPage = 10;
 


   const handlePageChange = (page) => {
        setCurrentPage(page);
   }


   const handleDelete = (id) => {
        axios
        .delete('http://localhost:8000/api/customers/' + id)
        .then(response => response.data)
        .catch(error => console.log(error.response))
   }

    useEffect(() => {
        axios
        .get(`http://localhost:8000/api/customers?pagination=true&count=${itemsPerPage}&page=${currentPage}`)
        .then(response => {
            setCustomers(response.data['hydra:member']);
            setTotalItems(response.data['hydra:totalItems']);
            setLoading(false);
        })
            
        
        .catch(error => console.log(error.response));
       
    }, [currentPage])
    return ( 
       <>
        <h1>Clients (pagination)</h1>
        <div className="container">
        <table className='table table-hover my-5'>
            <thead className='bg-dark text-light'>
                <tr>
                    <th scope='col'>Identifiant</th>
                    <th scope='col'>Client</th>
                    <th scope='col'>Email</th>
                    <th scope='col'>Entreprise</th>
                    <th scope='col' className='text-center'>Factures</th>
                    <th scope='col' className='text-center'>Total</th>
                    <th scope='col' className='text-center'>Actions</th>
                </tr>
            </thead>
            <tbody>
                {loading && (
                        <tr>
                            <td> Chargement...</td>
                        </tr>
                )}
                {!loading && 
                customers.map(customer =>  
                <tr key={customer.id}>
                        <th scope='row'>{customer.id}</th>
                         <td>
                             <a href="#">{customer.firstName} {customer.lastName}</a>
                         </td>
                         <td>{customer.email}</td>
                         <td>{customer.company}</td>
                         <td className='text-center'>
                             <span className="badge bg-dark">{customer.invoices.length}</span>
                         </td>
                         <td className='text-center'>{customer.totalAmount.toLocaleString()}€</td>
                         <td>
                             <a onClick={() => handleDelete(customer.id)} className={customer.invoices.length > 0 ? "btn btn-danger text-nowrap disabled" : "btn btn-danger text-nowrap"}><i className="bi bi-trash-fill"></i>&nbsp;Supprimer</a>
                         </td>
                </tr>
                )}
            </tbody>
        </table>
        <Pagination currentPage={currentPage} itemsPerPage={itemsPerPage} length={totalItems} onPageChanged={handlePageChange}/>
        </div>
       </>
     );
}
 
export default CustomersPageWithPagination;