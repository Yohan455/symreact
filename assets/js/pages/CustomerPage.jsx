import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Field from "../components/forms/Field";
import CustomersApi from "../services/CustomersApi";
import { toast } from "react-toastify";
import FormContentLoader from "../loaders/FormContentLoader";

const CustomerPage = ({match, history}) => {

  const {id = "new"} = match.params;

  
  const [customer, setCustomer] = useState({
    firstName: "",
    lastName: "",
    email: "",
    company: ""
  });

  const [errors, setErrors] = useState({
    firstName: "",
    lastName: "",
    email: "",
    company: ""
  });

  const [editing, setEditing] = useState(false);
  const [loading, setLoading] = useState(false);


  const fetchCustomer = async id => {
    try {
        const {firstName, lastName, email, company} = await CustomersApi.find(id)
        setCustomer({firstName, lastName, email, company});
        setLoading(false);
    } catch (error) {
        console.log(error.response);
        history.replace('/customers');
        
    }
   
  }

  // Chargement de customer si besoin au chargement du composant et au changement d'identifiant
  useEffect( () => {
    if (id !== "new") {
        setLoading(true);
        setEditing(true);
       fetchCustomer(id);
    }

  }, [id]);

  
  // Changement des inputs dans le formulaire
  const handleChange = ({ currentTarget }) => {
    const {name, value} = currentTarget;
    setCustomer({ ...customer, [name]: value });
  };

  // Gestion de la soumission
  const handleSubmit = async event => {
        event.preventDefault();

        try {
            if (editing) {
                 await CustomersApi.update(id, customer);
                 toast.success('Le client a bien été modifié')
            } else {
                 await CustomersApi.create(customer);

                 toast.success('Le client a bien été créé !')
                 setErrors({});
                 history.replace('/customers');
            }
        } catch ({response}) {
            const {violations} = response.data;
            if (violations) {
                const apiErrors = {};

                violations.forEach(({propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });

                setErrors(apiErrors);
            }
            toast.error('Il semble y avoir quelque chose qui cloche 🤨')
        }
        
  };
  return (
    <>
      <div className="container py-5">
        {!editing && <h1>Creation d'un client</h1> || <h1>Modification du client n°{id}</h1>}
        {loading && <FormContentLoader />}
        {!loading && (<form onSubmit={handleSubmit}>
            <Field
            name="firstName"
            label="Prénom"
            placeholder="John"
            value={customer.firstName}
            onChange={handleChange}
            error={errors.firstName}
            />
            <Field
            name="lastName"
            label="Nom de famille"
            placeholder="Doe"
            value={customer.lastName}
            onChange={handleChange}
            error={errors.lastName}
            />
            <Field
            name="email"
            label="Adresse email"
            type="email"
            placeholder="user@gmail.com"
            value={customer.email}
            onChange={handleChange}
            error={errors.email}
            />
            <Field
            name="company"
            label="Entreprise"
            placeholder="Enterprise .inc"
            value={customer.company}
            onChange={handleChange}
            error={errors.company}
            />
            <div className="form-group d-flex justify-content-center">
                <button className="btn btn-primary" type="submit">
                   {!editing && 'Créer' || 'Modifier'} 
                </button>
                <Link to="/customers" className="btn btn-link">
                    Retour
                </Link>
            </div>
        </form>)}
      </div>
    </>
  );
};

export default CustomerPage;
