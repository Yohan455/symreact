import React, {useState,useEffect} from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Pagination from '../components/Pagination';
import TableLoader from '../loaders/Tableloader';
import CustomersApi from '../services/CustomersApi';


const CustomersPage = () => {

   const [customers, setCustomers] = useState([]);
   const [currentPage, setCurrentPage] = useState(1);
   const [loading, setLoading] = useState(true);
   const [search, setSearch] = useState('');
   const itemsPerPage = 10;
 
   // gestion de la barre de recherche
   const handleSearch = ({currentTarget}) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
   }


   // Gestion du changement de page 
   const handlePageChange = page =>  setCurrentPage(page);
   


   // Gestion de la suppression d'un client 
   const handleDelete = async id => {
    const originalsCustomers = [...customers];

    setCustomers(customers.filter(customer => customer.id !== id));

    try {
        await CustomersApi.delete(id)
        toast.success('Le client a bien été supprimé !')
    } catch (error) {
        setCustomers(originalsCustomers);
        toast.error("le client n'a pas pu être supprimé")
    }
   }

   // Permet d'aller recuperer les customers 
   const fetchCustomers = async () => {
        try {
            const data = await CustomersApi.findAll();
            setCustomers(data);
            setLoading(false);
        } catch (error) {
            console.log(error.response);
            toast.error('Impossible de charger les clients')
        }
   }

   // Au chargement du composant on va recuperer les customers 
    useEffect(() => {
            fetchCustomers();
    }, [])

    // filtrage des customers en fonction de la recherche
    const filteredCustomers = customers.filter(
        c =>
            c.firstName.toLowerCase().includes(search.toLowerCase()) ||
            c.lastName.toLowerCase().includes(search.toLowerCase())   ||
            c.email.toLowerCase().includes(search.toLowerCase())    ||
            (c.company && c.company.toLowerCase().includes(search.toLowerCase()))
    );

    // Pagination des données
    const paginatedCustomers = Pagination.getData(filteredCustomers, currentPage, itemsPerPage);

    return ( 
       <>
        
        <div className="container py-5">
        <div className="d-flex justify-content-between align-items-center mb-3">
            <h1>Clients</h1>
            <Link className='btn btn-primary ' to={'/customers/new'}>Créer un client</Link>
        </div>
        <div className="form-group">
            <input type="text" onChange={handleSearch} value={search} className="form-control search" placeholder="🔍 Rechercher..." />
        </div>
        {loading && <TableLoader />}
        {!loading &&
        <table className='table table-hover my-5'>
            <thead className='bg-dark text-light'>
                <tr>
                    <th scope='col'>Identifiant</th>
                    <th scope='col'>Client</th>
                    <th scope='col'>Email</th>
                    <th scope='col'>Entreprise</th>
                    <th scope='col' className='text-center'>Factures</th>
                    <th scope='col' className='text-center'>Total</th>
                    <th scope='col' className='text-center'>Actions</th>
                </tr>
            </thead>
            <tbody>
                {paginatedCustomers.map(customer =>  
                <tr key={customer.id}>
                        <th scope='row'>{customer.id}</th>
                         <td>
                             <Link to={"/customers/" + customer.id}>{customer.firstName} {customer.lastName}</Link>
                         </td>
                         <td>{customer.email}</td>
                         <td>{customer.company}</td>
                         <td className='text-center'>
                             <span className="badge bg-dark">{customer.invoices.length}</span>
                         </td>
                         <td className='text-center'>{customer.totalAmount.toLocaleString()}€</td>
                         <td>
                             <a onClick={() => handleDelete(customer.id)} className={customer.invoices.length > 0 ? "btn btn-danger disabled" : "btn btn-danger"}><i className="bi bi-trash-fill"></i>&nbsp;Supprimer</a>
                         </td>
                </tr>
                )}
            </tbody>
        </table>}
        
        {itemsPerPage < filteredCustomers.length && 
            <Pagination currentPage={currentPage} 
            itemsPerPage={itemsPerPage} 
            length={filteredCustomers.length} 
            onPageChanged={handlePageChange}/>
        }
        </div>
       </>
     );
}
 
export default CustomersPage;