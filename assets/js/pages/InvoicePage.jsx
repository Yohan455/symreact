import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Field from '../components/forms/Field';
import Select from '../components/forms/Select';
import FormContentLoader from '../loaders/FormContentLoader';
import CustomersApi from '../services/CustomersApi';
import InvoicesApi from '../services/InvoicesApi';


const InvoicePage = ({history, match}) => {

    const { id = "new" } = match.params;

    const [invoice, setInvoice] = useState({
        amount: "",
        customer: "",
        status: "SENT"
    });
    const [customers, setCustomers] = useState([]);

    const [errors, setErrors] = useState({
        amount: "",
        customer: "",
        status: "",
    });
    const [loading, setLoading] = useState(true);
    const [editing, setEditing] = useState(false);

    // Changement des inputs dans le formulaire
    const handleChange = ({ currentTarget }) => {
        const {name, value} = currentTarget;
        setInvoice({ ...invoice, [name]: value });
    };

    const fetchCustomers = async () => {
        try {
            const data = await CustomersApi.findAll();
            setCustomers(data);
            setLoading(false);
            if(!invoice.customer) setInvoice({ ...invoice, customer: data[0].id });
        } catch (error) {
            console.log(error.response);
            toast.error('Il semble y avoir quelque chose qui cloche 🤨')
            history.replace('/invoices');
        }
    }

   

    const fetchInvoice = async id => {
        try {
            const {amount, customer, status} = await InvoicesApi.find(id);
            setInvoice({amount, customer: customer.id, status});
            setLoading(false);
        } catch (error) {
            console.log(error.response);
            toast.error('Il semble y avoir quelque chose qui cloche 🤨')
            history.replace('/invoices');
        }
    }

    useEffect(() => {
        fetchCustomers();
    }, []);

    useEffect(() => {
        if(id !== 'new') {
            setEditing(true);
            fetchInvoice(id);
        }
        
    }, [id]);

    

    const handleSubmit = async event => {
        event.preventDefault();
        try {
            if (editing) {
               await InvoicesApi.update(id, invoice);
                toast.success('La facture a bien été modifiée !');
            } else {
                await InvoicesApi.create(invoice);
                toast.success('La facture a bien été créée !');
                history.replace('/invoices');
            }
        } catch ({response}) {
            const {violations} = response.data;
            if (violations) {
                const apiErrors = {};

                violations.forEach(({propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });

                setErrors(apiErrors);
            }
            toast.error('Il semble y avoir quelque chose qui cloche 🤨')
        }
    }

    return ( 
        <>
        <div className="container py-5">
             {editing && <h1>Modification d'une Facture</h1> || <h1>Création de Facture</h1>}
             {loading && <FormContentLoader />}
             {!loading && (<form onSubmit={handleSubmit}>
             <Field
                name="amount"
                label="Montant"
                placeholder="100.00€"
                value={invoice.amount}
                onChange={handleChange}
                error={errors.amount}
                />
                <Select name="customer" 
                        label="Client" 
                        value={invoice.customer}
                        error={errors.customer}
                        onChange={handleChange}
                >
                {customers.map(customer => 
                   <option key={customer.id} value={customer.id}>
                        {customer.firstName} {customer.lastName}
                    </option>
                )}

                </Select>
                <Select name="status" 
                        label="Statut" 
                        value={invoice.status}
                        error={errors.status}
                        onChange={handleChange}
                >
                   <option value="SENT">Envoyée</option>
                   <option value="PAID">Payée</option>
                   <option value="CANCELLED">Annulée</option>

                </Select>
                <div className="form-group d-flex justify-content-center mb-1">

                    <button type="submit" className='btn btn-success'>{editing && 'Modifier' || 'Enregistrer'}</button>
                    <Link to="/invoices" className='btn btn-link'>Retour aux factures</Link>
                </div>
             </form>)}
        </div>
        </>
     );
}
 
export default InvoicePage;