import axios from 'axios';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Field from '../components/forms/Field';
import { USERS_API } from '../config';


const RegisterPage = ({ history }) => {

    const [user, setUser] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        passwordConfirm: ""
    });

    const [errors, setErrors] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        passwordConfirm: ""
    });

    const handleChange = ({ currentTarget }) => {
        const {name, value} = currentTarget;
        setUser({ ...user, [name]: value });
    };

    const handleSubmit = async event => {
        event.preventDefault();

        const apiErrors = {};

        if (user.password !== user.passwordConfirm) {
            apiErrors.passwordConfirm = 'Le mots de passe et la confirmation doivent être identiques';
            setErrors(apiErrors);
            toast.error('Il semble y avoir quelque chose qui cloche 🤨')
            return;
        }
        try {
            const response = await axios.post(USERS_API, user);
            console.log(response);
            setErrors({});
            toast.success("Bienvenue à vous, connectez-vous et accedez à votre compte !")
            history.replace('/login');
            
        } catch (error) {
            const {violations} = error.response.data;
            if (violations) {
            

                violations.forEach(({propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });

                setErrors(apiErrors);
                toast.error('Il semble y avoir quelque chose qui cloche 🤨')
            }
        }
    }

    return ( 
        <>
            <h1 className='text-center'>Inscription</h1>
            <div className="container py-5">
                <div className="d-flex justify-content-center">
            <form className='w-50' onSubmit={handleSubmit}>
                <Field
                    name="firstName"
                    label="Prenom"
                    placeholder="John"
                    value={user.firstName}
                    onChange={handleChange}
                    error={errors.firstName}
                    />
                    <Field
                    name="lastName"
                    label="Nom"
                    placeholder="Snow"
                    value={user.lastName}
                    onChange={handleChange}
                    error={errors.lastName}
                    />
                    <Field
                    name="email"
                    label="Adresse email"
                    placeholder="john.snow@gmail.com"
                    value={user.email}
                    onChange={handleChange}
                    error={errors.email}
                    />
                    <Field
                    name="password"
                    label="Mot de passe"
                    placeholder="********"
                    type='password'
                    value={user.password}
                    onChange={handleChange}
                    error={errors.password}
                    />
                    <Field
                    name="passwordConfirm"
                    label="Confirmation du mot de passe"
                    type='password'
                    placeholder="********"
                    value={user.passwordConfirm}
                    onChange={handleChange}
                    error={errors.passwordConfirm}
                    />
                     <div className="form-group mb-3">
                        <div className="d-grid gap-2">
                            <button className="btn btn-primary" type="submit">
                            S'enregistrer
                            </button>
                        </div>
                        <p className="text-center mt-3">
                            <Link to='/login' class="btn btn-link">
                            J'ai déjà un compte
                            </Link>
                        </p>
                    </div>
            </form>
            </div>    
            </div>
        </>
     );
}
 
export default RegisterPage;