import React from "react";

const Field = ({ name, label, value, placeholder = '', onChange, type = 'text', error = '' }) => 
   (
    <div className="form-group mb-3">
      <label htmlFor={name}>{label}</label>
      <input
        onChange={onChange}
        value={value}
        type={type}
        name={name}
        id={name}
        className={"form-control " + (error && "is-invalid")}
        placeholder={placeholder || label}
      />
      {error && <p className="invalid-feedback small">{error} </p>}
    </div>
  );

export default Field;
