import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { toast } from "react-toastify";
import AuthContext from "../context/AuthContext";
import AuthApi from "../services/AuthApi";


const Navbar = ({ history }) => {

  const {isAuthenticated, setIsAuthenticated} = useContext(AuthContext);

  const handleLogout = () => {
    AuthApi.Logout();
    setIsAuthenticated(false);
    toast.info("À bientôt 🤙 👋 !");
    history.push('/login');
    
  };
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to={"/"}>
          SymReact
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarColor01"
          aria-controls="navbarColor01"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarColor01">
          <ul className="navbar-nav me-auto">
            <li className="nav-item">
              <NavLink className="nav-link" to={"/customers"}>
                Clients
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={"/invoices"} className="nav-link" >
                Factures
              </NavLink>
            </li>
          </ul>
          <ul className="navbar-nav ml-auto">
            {(!isAuthenticated && (
              <>
                <li className="nav-item">
                  <NavLink to={"/login"} className="btn btn-light">
                    <i className="bi bi-person-fill"></i>&nbsp;Connexion
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to={"/register"} className="nav-link">
                    <i className="bi bi-person-fill-add"></i>&nbsp;inscription
                  </NavLink>
                </li>
              </>
            )) || (
              <li className="nav-item">
                <button onClick={handleLogout} className="btn btn-light">
                  <i className="bi bi-door-closed-fill"></i>&nbsp;Deconnexion
                </button>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
