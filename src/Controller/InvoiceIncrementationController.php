<?php 

namespace App\Controller;

use App\Entity\Invoice;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceIncrementationController extends AbstractController
{

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route(path:'/api/invoices/{id}/increment',
             name:'invoice_increment', 
             methods:['POST']
             )]
    public function __invoke(Invoice $data) 
    {
        $data->setChrono($data->getChrono() + 1);

        $this->entityManager->flush();

        return $this->json($data);
    }
}