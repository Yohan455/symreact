<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\InvoiceRepository;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Config\ApiPlatform\SwaggerConfig;
use Symfony\Component\Validator\Constraints as Assert; 


#[ApiResource(
    paginationEnabled: false,
    paginationItemsPerPage: 20,
    order: ['sentAt' => 'DESC'],
    normalizationContext:["groups" => ["invoices_read"]],
    denormalizationContext:["disable_type_enforcement" => [true]],
    operations:[
        new Get(),
        new GetCollection(),
        new Put(),
        new Post(),
        new Delete(),
        new Post(name: 'increment',
                 routeName:'invoice_increment',
                 uriTemplate:'/api/invoices/{id}/increment',
                 requirements: ['id' => '\d+'],
                 controller: 'App\Controller\InvoiceIncrementationController',
                 description:'Increment an invoice chrono',
                 openapiContext:['summary' => 'Increment an invoice chrono', 
                                 'description' => 'It will increment the chrono for the mentionned invoice'
                ],
                normalizationContext:["groups" => ["invoices_increment"]]

                )]
)]

#[ApiResource(
    uriTemplate: '/customers/{id}/invoices', 
    uriVariables: [
        'id' => new Link(
            fromClass: Customer::class,
            fromProperty: 'invoices',
        )
    ], 
    operations: [new GetCollection()],
    normalizationContext:["groups" => ["invoices_subresource"]]
)]
#[ApiFilter(OrderFilter::class, properties: ['amount','sentAt'])]
#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
class Invoice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['invoices_read', 'customers_read', 'invoices_subresource', 'invoice_increment'])]
    private $id;
    #[ORM\Column(type: 'float')]
    #[Groups(['invoices_read', 'customers_read', 'invoices_subresource', 'invoice_increment'])]
    #[Assert\NotBlank(message:'Le montant de la facture est obligatoire')]
    #[Assert\Type(type:'numeric', message:'Le montant de la facture doit être numérique')]
    private $amount;
    #[ORM\Column(type: 'datetime')]
    #[Groups(['invoices_read', 'customers_read', 'invoices_subresource', 'invoice_increment'])]
    #[Assert\Type(type:'datetime', message:'La date doit être au format YYYY-MM-DD')]
    #[Assert\NotBlank(message:'La date d\'envoi est obligatoire')]
    private $sentAt;
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['invoices_read', 'customers_read', 'invoices_subresource', 'invoice_increment'])]
    #[Assert\NotBlank(message:'Le statut de la facture est obligatoire')]
    #[Assert\Choice(choices:["SENT","PAID","CANCELLED"], message:"le statut doit être 'SEND' 'PAID' ou 'CANCELLED'")]
    private $status;
    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'invoices')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['invoices_read'])]
    #[Assert\NotBlank(message:'Le client de la facture est obligatoire')]
    private $customer;
    #[ORM\Column(type: 'integer')]
    #[Groups(['invoices_read', 'customers_read', 'invoices_subresource', 'invoice_increment'])]
    #[Assert\NotBlank(message:'Le chrono de la facture est obligatoire')]
    #[Assert\Type(type:'integer' ,message:'Le type du chrono doit être un nombre')]
    private $chrono;
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * Permet de récupérer le user qui appartient à un customer
     * @return User
     */
    #[Groups(['invoices_read', 'invoices_subresource', 'invoice_increment'])]
    public function getUser(): User
    {
        return $this->customer->getUser();
    }
    public function getAmount() : ?float
    {
        return $this->amount;
    }
    public function setAmount($amount) : self
    {
        $this->amount = $amount;
        return $this;
    }
    public function getSentAt() : ?\DateTimeInterface
    {
        return $this->sentAt;
    }
    public function setSentAt($sentAt) : self
    {
        $this->sentAt = $sentAt;
        return $this;
    }
    public function getStatus() : ?string
    {
        return $this->status;
    }
    public function setStatus(string $status) : self
    {
        $this->status = $status;
        return $this;
    }
    public function getCustomer() : ?Customer
    {
        return $this->customer;
    }
    public function setCustomer(?Customer $customer) : self
    {
        $this->customer = $customer;
        return $this;
    }
    public function getChrono() : ?int
    {
        return $this->chrono;
    }
    public function setChrono($chrono) : self
    {
        $this->chrono = $chrono;
        return $this;
    }
}
