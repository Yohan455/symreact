<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CustomerRepository;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; 

#[ApiResource(
    normalizationContext:["groups" => ["customers_read"]],
    operations:[
        new Get(
            requirements: ['id' => '\d+']
        ),
        new GetCollection(),
        new Post(),
        new Delete(),
        new Put()
    ]
    
)]
#[ApiFilter(SearchFilter::class)] 
#[ApiFilter(OrderFilter::class)]
#[ORM\Entity(repositoryClass: CustomerRepository::class)]
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['customers_read', 'invoices_read'])]
    private $id;
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['customers_read', 'invoices_read'])]
    #[Assert\NotBlank(message:'Le Prénom du client est obligatoire')]
    #[Assert\Length(min:3, minMessage:'Le prenom doit faire au moins 3 caractères', max:255, maxMessage:'Le prenom ne doit pas dépasser 255 caractères')]
    private $firstName;
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['customers_read', 'invoices_read'])]
    #[Assert\NotBlank(message:'Le Nom du client est obligatoire')]
    #[Assert\Length(min:3, minMessage:'Le nom doit faire au moins 3 caractères', max:255, maxMessage:'Le nom ne doit pas dépasser 255 caractères')]
    private $lastName;
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['customers_read', 'invoices_read'])]
    #[Assert\NotBlank(message:'Le mail du client est obligatoire')]
    #[Assert\Email(message:'Merci de fournir un email au format valide')]
    #[Assert\Length(max:255, maxMessage:'L\'email ne peut dépasser 255 caractères')]
    private $email;
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['customers_read', 'invoices_read'])]
    private $company;
    #[ORM\OneToMany(targetEntity: Invoice::class, mappedBy: 'customer')]
    #[Groups(['customers_read'])]
    private $invoices;
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'customers')]
    #[Groups(['customers_read'])]
    #[Assert\NotBlank(message:'L\'utilisateur du client est obligatoire')]
    private $user;
    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }

    /**
     * Permet de recupérer le total des invoices
     * @return float
     */
    #[Groups(['customers_read'])]
    public function getTotalAmount(): float
    {
       return array_reduce($this->invoices->toArray(), function($total, $invoice){
            return $total + $invoice->getAmount();
       },0);
    }

    /**
     * Permet de recupérer le total des invoices non payées
     * @return float
     */
    #[Groups(['customers_read'])]
    public function getUnpaidAmount(): float
    {
        return array_reduce($this->invoices->toArray(), function($total, $invoice){
            return $total + ($invoice->getStatus() === "PAID" || $invoice->getStatus() === "CANCELLED" ? 0 : $invoice->getAmount());
        },0);
    }

    public function getId() : ?int
    {
        return $this->id;
    }
    public function getFirstName() : ?string
    {
        return $this->firstName;
    }
    public function setFirstName(string $firstName) : self
    {
        $this->firstName = $firstName;
        return $this;
    }
    public function getLastName() : ?string
    {
        return $this->lastName;
    }
    public function setLastName(string $lastName) : self
    {
        $this->lastName = $lastName;
        return $this;
    }
    public function getEmail() : ?string
    {
        return $this->email;
    }
    public function setEmail(string $email) : self
    {
        $this->email = $email;
        return $this;
    }
    public function getCompany() : ?string
    {
        return $this->company;
    }
    public function setCompany(?string $company) : self
    {
        $this->company = $company;
        return $this;
    }
    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices() : Collection
    {
        return $this->invoices;
    }
    public function addInvoice(Invoice $invoice) : self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setCustomer($this);
        }
        return $this;
    }
    public function removeInvoice(Invoice $invoice) : self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getCustomer() === $this) {
                $invoice->setCustomer(null);
            }
        }
        return $this;
    }
    public function getUser() : ?User
    {
        return $this->user;
    }
    public function setUser(?User $user) : self
    {
        $this->user = $user;
        return $this;
    }
}
